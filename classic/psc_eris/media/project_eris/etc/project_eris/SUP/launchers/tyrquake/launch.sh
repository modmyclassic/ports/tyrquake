#!/bin/sh
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
chmod +x "tyr-quake"
echo -n 2 > "/data/power/disable"


${PROJECT_ERIS_PATH}/bin/sdl_input_text_display " " 0 0 12 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/quake_controller_select.png" XO

SELECTEDOPTION=$?

if [ "${SELECTEDOPTION}" = "100" ]; then
  # Set up controller config according to only one DPAD
  sed -i -e 's/bind "GCLEFT" "[^"]*"/bind "GCLEFT" "+left"/g' ".tyrquake/id1/config.cfg"
  sed -i -e 's/bind "GCRIGHT" "[^"]*"/bind "GCRIGHT" "+right"/g' ".tyrquake/id1/config.cfg"
elif [ "${SELECTEDOPTION}" = "101" ]; then
  # Set up controller config as we have analouge sticks
  sed -i -e 's/bind "GCLEFT" "[^"]*"/bind "GCLEFT" "+moveleft"/g' ".tyrquake/id1/config.cfg"
  sed -i -e 's/bind "GCRIGHT" "[^"]*"/bind "GCRIGHT" "+moveright"/g' ".tyrquake/id1/config.cfg"
else
  echo "[ERROR] Couldn't detect controller selection result. RESULT CODE: ${SELECTEDOPTION}" > "${RUNTIME_LOG_PATH}/lzdoom_brutal.log"
  exit 1
fi

HOME="/var/volatile/launchtmp" ./tyr-quake -gamecontrollerdb gamecontrollerdb.txt -width 1280 -height 720 &> "${RUNTIME_LOG_PATH}/quake.log"
echo -n 1 > "/data/power/disable"
echo "launch_StockUI" > "/tmp/launchfilecommand"